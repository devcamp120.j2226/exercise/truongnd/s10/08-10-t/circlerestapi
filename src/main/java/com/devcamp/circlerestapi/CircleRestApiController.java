package com.devcamp.circlerestapi;
import java.util.ArrayList;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CircleRestApiController {
    @CrossOrigin
    @GetMapping("/circle-area")
    public ArrayList<Circle> circleItems(){
        Circle hinhtron1 = new Circle();
        Circle hinhtron2 = new Circle(2.0);

        System.out.println(hinhtron1.toString());
        System.out.println(hinhtron2.toString());

        System.out.println(hinhtron1.getArea());
        System.out.println(hinhtron2.getArea());

        ArrayList<Circle> circleArray = new ArrayList <>();
        circleArray.add(hinhtron1);
        circleArray.add(hinhtron2);

        return circleArray;
    }
}
